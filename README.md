How to use:

# 1 Take a photo or choose from your gallery. Enable flashlight if needed.
![1.png](img/1.png)
# 2 Select style.
![2.png](img/2.png)
# 3 Wait. Magic is happening.
![3.png](img/3.png)
# 4 Adjust intensity of style.
![4.png](img/4.png)
![5.png](img/5.png)
# 5 Share photo
![6.png](img/6.png)